package api
import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"main/model"
	"main/utils"
)

func CreateConfig(w http.ResponseWriter, r *http.Request) {
	var e model.ConfigBody
	var unmarshalErr *json.UnmarshalTypeError

	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&e)
	if err != nil {
		if errors.As(err, &unmarshalErr) {
			utils.ErrorResponse(w, "Bad Request. Wrong Type provided for field "+unmarshalErr.Field, http.StatusBadRequest)
		} else {
			utils.ErrorResponse(w, "Bad Request "+err.Error(), http.StatusBadRequest)
		}
		return
	}

	path := e.Filename + ".conf"
    file, err := os.Create(path)
    if err != nil {
        log.Fatal(err)
    }
	os.WriteFile(
		file.Name(), 
		[]byte(
			utils.CreateConfigBody(e.Clusters, e.Servers)), 0644)
    fmt.Println("File created successfully")
	w.Header().Set("Content-Disposition", "attachment; filename="+ file.Name())
	w.Header().Set("Content-Type", "application/xml")
	io.Copy(w, file)
}