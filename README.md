# Nginx Config Gen

Disclaimer: This repository has to be intended as a peersonal exercise for learning Golang by developing a real-world application (not the usual hello world).
Nginx is one of the most commonly used proxy server, with a variety of application such as Web Server, Kubernetes Ingress and so on.
Nginx Config Gen is a no-code application that enables you to generate an nginx configuration from a JSON file.
In order to exercise with http connections, Nginx Config Gen expose a REST API that can be used for generating the config file given a JSON body.
As a result, the aforementioned API returns the configuration as a downloadable file.


## API reference
### Create a new configuration
- PATH:/ 
- VERB: POST
- HEADERS: none
- BODY: JSON, with the following properties: 
    - filename (string): name of the config file that will be returned
    - clusters (list of objects): define a list of cluster that will be instantiated in the config file. Every cluster is structured as:
        - name (string): name of the cluster
        - weightedUrls (list of objects): cluster urls and relative weights. Every object is structured as:
            - url (string)
	        - weight (int)
    - servers (list of objects): define a set of servers. Every server is structured as:
        - port (int)
	    - name (string)
	    - locations (list of objects): define a set of location that nginx will expose on the given server. Every location is structured as:
            - uri (string): base uri to which the request will be forwarded. If it is used inside a container environment, for example you could use the container/pod names 
	        - redirectPath (string)
- RETURNS: 
    - 200: the request was successfully fulfilled and the config is returned as the body (downloadable) of the response
    - 400: missing mandatory parameters

## Collaboration

If you are willing to contribute to this project, feel free to open new issues or contributing and creating PR.
Thanks.

Luca