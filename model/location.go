package model

type ProxyLocation struct {
	Uri string `json:"uri"`
	RedirectPath string `json:"redirectPath"`
}