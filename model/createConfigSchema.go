package model

type ConfigBody struct {
	Filename string `json:"filename"`
	Servers []Server `json:"servers"`
	Clusters []Cluster `json:"clusters"`
}