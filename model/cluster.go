package model

type Cluster struct {
	Name string `json:"name"`
	WeightedUrls []WeightedUrl `json:"weightedUrls"`
}

type WeightedUrl struct {
	Url string `json:"url"`
	Weight int `json:"weight"`
}