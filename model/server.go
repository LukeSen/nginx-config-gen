package model

type Server struct {
	Port int `json:"port"`
	Name string `json:"name"`
	ReverseProxy []ProxyLocation `json:"locations"`
}