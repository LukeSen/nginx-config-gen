package utils
import(
	"main/model"
	"fmt"
)

func BuildCluster(cluster model.Cluster) string {
	clusterConfig := "\tupstream " + cluster.Name + " {\n"
	for _, weightedUrl := range cluster.WeightedUrls {
		clusterConfig += "\t\tserver " + weightedUrl.Url 
		if weightedUrl.Weight > 0 {
			clusterConfig += " weight=" + fmt.Sprint(weightedUrl.Weight) 
		} 
		clusterConfig += ";\n"
	}
	clusterConfig += "\t}\n"
	return clusterConfig
}