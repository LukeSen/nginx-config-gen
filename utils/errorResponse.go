package utils
import ( "net/http" )
func ErrorResponse(w http.ResponseWriter, message string, httpStatusCode int) {
	w.WriteHeader(httpStatusCode)
	w.Write([]byte(message))
}