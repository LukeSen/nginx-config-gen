package utils
import (
	"main/model" 
)

func CreateLocationConfig(reverseProxy model.ProxyLocation) string {
	locationTemplate := "\t" + "location " + reverseProxy.Uri + " {\n"
	locationTemplate += "\t\t\tproxy_pass " + reverseProxy.RedirectPath + "\n"
	locationTemplate += "\t\t}\n"
	return locationTemplate
}