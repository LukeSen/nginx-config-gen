package utils

import (
	"main/model"
)

func CreateConfigBody(clusters []model.Cluster, servers []model.Server) string {
	body := "user       www www;\n"
	body += "worker_processes  5;\n"
	body += "error_log  logs/error.log;\n"
	body += "pid        logs/nginx.pid;\n"
	body += "worker_rlimit_nofile 8192;\n\n"
	body += "events {\n"
	body += "\tworker_connections  4096;\n"
	body += "}\n"
	body += "\nhttp {\n"
	body += "\tdefault_type application/octet-stream;\n"
	body += "\tlog_format   main '$remote_addr - $remote_user [$time_local]  $status '\n"
	body += "\t\t'\"$request\" $body_bytes_sent \"$http_referer\" '\n"
	body += "\t\t'\"$http_user_agent\" \"$http_x_forwarded_for\"';\n"
	body += "\taccess_log   logs/access.log  main;\n"
	body += "\tsendfile     on;\n"
	body += "\ttcp_nopush   on;\n"
	body += "\tserver_names_hash_bucket_size 128;\n"
	for _, cluster := range clusters {
		body += BuildCluster(cluster)
	}
	for _, server := range servers {
		body += CreateServer(server)
	}
	body += "\n}\n"
	return body
}
