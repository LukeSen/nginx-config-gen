package utils

import (
	"fmt"
	"main/model"
)

func CreateServer(server model.Server) string {
	serverConfig := "\tserver {\n"
	serverConfig += "\t\tlisten       " + fmt.Sprint(server.Port) + ";\n"
	serverConfig += "\t\tserver_name  " + server.Name + ";\n"
	serverConfig += "\t\taccess_log   logs/" + server.Name + ".access.log  main;\n"
	for _, revProxy := range server.ReverseProxy {
		serverConfig += "\t" + CreateLocationConfig(revProxy)
	}
	serverConfig += "\t}\n"
	return serverConfig
}
